package class01;

import java.util.Arrays;

public class Code01_SelectionSort {

    public static void selectionSort(int[] arr) {
        if (arr == null || arr.length < 2) {
            return;
        }
        for (int i = 0; i < arr.length; i++) {
            int minIndex = i;
            for (int j = i + 1; j < arr.length; j++) {
                minIndex = arr[j] < arr[minIndex] ? j : minIndex;
            }
            swap(arr, i, minIndex);
        }
    }

    private static void swap(int[] arr, int i, int minIndex) {
        int temp = arr[i];
        arr[i] = arr[minIndex];
        arr[minIndex] = temp;
    }

    public static void main(String[] args) {
        int[] ints = {4, 5, 6, 3, 3, 2, 1};
        Arrays.stream(ints).forEach(System.out::print);
        System.out.println();
        selectionSort(ints);
        Arrays.stream(ints).forEach(System.out::print);
        System.out.println();
    }
}
