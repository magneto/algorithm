# Algorithm

#### 介绍
程序员代码面试指南  
LeetCode  
剑指offer  
编程之美  
左程云算法新手班  

程序员代码面试指南、LeetCode先整整，其他的还没开始  



#### 关于leetcode插件
IDEA leetcode插件安装后设置用户名
设置模板信息

```
${question.content}
${question.title}
package editor.cn;
public class P${question.frontendQuestionId}$!velocityTool.camelCaseName(${question.titleSlug}){
    public static void main(String[] args) {
        Solution solution = new P$!{question.frontendQuestionId}$!velocityTool.camelCaseName(${question.titleSlug})().new Solution();
        // TO TEST
    }
    ${question.code}
}
```

这两个注释之间的代码是要提交的代码
两行注释不能去掉
``` 
 //leetcode submit region begin(Prohibit modification and deletion)
 class Solution {
     public int[] twoSum(int[] nums, int target) {
         return null;
     }
 }
 //leetcode submit region end(Prohibit modification and deletion)
```
 
新建一个普通java项目
打开一个题目 问题1 两数之和
发现新文件的路径为leetcode.editor.cn
open module settings  
将leetcode设置为  source目录，src目录放弃设置为source
改改代码的小毛病 开始写代码
![输入图片说明](https://images.gitee.com/uploads/images/2020/0806/094224_a44a0635_336355.png "QQ截图20200806094200.png")
文件全提交吧 
.idea也提上去吧
gitignore不配置了
