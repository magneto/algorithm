package top.pro51.guide.chapter1;

import java.util.Stack;

public class 由两个栈组成的队列 {
    private Stack<Integer> stackPush;
    private Stack<Integer> stackPop;

    public 由两个栈组成的队列() {
        this.stackPush = new Stack<Integer>();
        this.stackPop = new Stack<Integer>();
    }

    //转移 向pop倒入数据
    private void pushToPop() {
        if (stackPop.empty()) {
            while (!stackPush.empty()) {
                stackPop.push(stackPush.pop());
            }
        }
    }

    public void add(int pushNum) {
        stackPush.push(pushNum);
        pushToPop();
    }

    public int poll() {
        if (stackPop.empty() && stackPush.empty()) {
            throw new RuntimeException("empty Stack");
        }
        pushToPop();
        return stackPop.pop();
    }

    public int peek() {
        if (stackPop.empty() && stackPush.empty()) {
            throw new RuntimeException("empty Stack");
        }
        pushToPop();
        return stackPop.peek();
    }

}
