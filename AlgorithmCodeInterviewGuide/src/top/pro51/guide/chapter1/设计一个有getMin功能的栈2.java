package top.pro51.guide.chapter1;

import java.util.Stack;

public class 设计一个有getMin功能的栈2 {
    private Stack<Integer> stackData;
    private Stack<Integer> stackMin;

    public 设计一个有getMin功能的栈2() {
        this.stackData = new Stack<Integer>();
        this.stackMin = new Stack<Integer>();
    }
    public void push(int newNum) {
        if (this.stackMin.empty()) {
            this.stackMin.push(newNum);
        } else if (newNum < this.getMin()) {
            this.stackMin.push(newNum);
        }
        this.stackData.push(newNum);
    }

    public int pop() {
        if (this.stackMin.empty()) {
            throw new RuntimeException("empty Stack");
        }
        int value = this.stackData.pop();
        if (value == this.getMin()) {
            this.stackMin.pop();
        }
        return value;
    }

    public int getMin() {
        if (this.stackMin.empty()) {
            throw new RuntimeException("empty Stack");
        }
        return this.stackMin.peek();
    }

}
