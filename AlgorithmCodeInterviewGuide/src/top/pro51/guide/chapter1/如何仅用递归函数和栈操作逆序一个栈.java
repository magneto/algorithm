package top.pro51.guide.chapter1;

import java.util.Stack;

/**
 * @ClassName 如何仅用递归函数和栈操作逆序一个栈
 * @Description:
 * @Author: WangWenpeng
 * @date: 10:08 2021/1/11
 * @Version 1.0
 */
public class 如何仅用递归函数和栈操作逆序一个栈 {

    /**
     * @Description stack的栈底元素返回并移除
     * @Author WangWenpeng
     * @Date 10:14 2021/1/11
     * @Param [stack]
     */
    public static int getAndRemoveLastElement(Stack<Integer> stack) {
        //弹出栈顶元素
        int result = stack.pop();
        //弹出元素后如果栈为空，则返回该元素
        if (stack.isEmpty()) {
            return result;
        } else {
            //不为空时，则递归。此时栈为原栈弹出栈顶元素后的一个变化的栈。
            //当递归到栈底元素时，将栈顶元素返回并赋值给变量last
            int last = getAndRemoveLastElement(stack);
            //递归结束。将除栈底元素的其他元素按原先顺序依次入栈。
            //此时的栈与原栈的区别是：栈底元素被移除
            stack.push(result);
            //返回原栈底元素
            return last;
        }
    }

    /**
     * @Description 逆序一个栈
     * @Author WangWenpeng
     * @Date 10:15 2021/1/11
     * @Param [stack]
     */
    public static void reverse(Stack<Integer> stack) {
        if (stack.isEmpty()) {
            return;
        }
        //得到栈底元素
        int i = getAndRemoveLastElement(stack);
        //递归。此时的栈是原栈返回并移除栈底元素的一个变化栈。
        reverse(stack);
        //递归结束。递归到栈空时，将得到的栈底元素依次入栈。
        stack.push(i);
    }

    public static void main(String[] args) {
        Stack s1 = new Stack();
        s1.push(5);
        s1.push(7);
        s1.push(9);
        System.out.println("The elements in the original stack:");
        for (int i = 0; i <= s1.size() + 1; i++) {
            System.out.println(s1.pop());
        }

        s1.push(5);
        s1.push(7);
        s1.push(9);
        reverse(s1);
        System.out.println("===================================");
        System.out.println("The elements in the changed stack:");
        for (int i = 0; i <= s1.size() + 1; i++) {
            System.out.println(s1.pop());
        }
    }
}
