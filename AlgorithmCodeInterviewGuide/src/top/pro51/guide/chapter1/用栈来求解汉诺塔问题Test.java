package top.pro51.guide.chapter1;


class 用栈来求解汉诺塔问题Test {
    public static void main(String[] args) {
        用栈来求解汉诺塔问题1 method1 = new 用栈来求解汉诺塔问题1();
        int step1 = method1.hanoiProblem(2, "left", "mid", "right");
        System.out.println("method1: " + step1);
        System.out.println("*-----------------------------------------------------------");

        用栈来求解汉诺塔问题2 method2 = new 用栈来求解汉诺塔问题2();
        int step2 = method2.hanoiProblem2(2, "left", "mid", "right");
        System.out.println("method2: " + step2);

    }
}
