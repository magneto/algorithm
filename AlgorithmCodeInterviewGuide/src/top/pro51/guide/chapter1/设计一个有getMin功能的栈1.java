package top.pro51.guide.chapter1;

import java.util.Stack;

public class 设计一个有getMin功能的栈1 {
    private Stack<Integer> stackData;
    private Stack<Integer> stackMin;

    public 设计一个有getMin功能的栈1() {
        this.stackData = new Stack<Integer>();
        this.stackMin = new Stack<Integer>();
    }

    //同步存入最小值
    public void push(int newNum) {
        if (this.stackMin.empty()) {
            this.stackMin.push(newNum);
        } else if (newNum < this.getMin()) {
            this.stackMin.push(newNum);
        } else {
            int newMin = this.stackMin.peek();
            this.stackMin.push(newMin);
        }
        this.stackData.push(newNum);
    }

    //两个栈同步取出
    public int pop() {
        if (this.stackMin.empty()) {
            throw new RuntimeException("empty Stack");
        }
        this.stackMin.pop();
        return this.stackData.pop();
    }

    public int getMin() {
        if (this.stackMin.empty()) {
            throw new RuntimeException("empty Stack");
        }
        return this.stackMin.peek();
    }
}
