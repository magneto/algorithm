package top.pro51.guide.chapter1;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @ClassName 猫狗队列
 * @Description:
 * @Author: WangWenpeng
 * @date: 10:21 2021/1/11
 * @Version 1.0
 */
public class 猫狗队列 {
    //实现一种猫狗队列结构，要求如下：
    //①用户可以调用add方法将cat类或dog类的实例放进队列中；
    //②用户可以调用pollAll方法， 将队列中所有的实例按照进队列的先后顺序依次弹出；
    //③用户可以调用pollDog方法，将队列中所有的Dog类的实例按照进队列的先后顺序依次弹出；
    //④用户可以调用pollCat方法，将队列中所有的Cat类的实例按照进队列的先后顺序依次弹出；
    //⑤用户可以调用isEmpty方法，检查队列里，是否还有Dog或Cat类的实例；
    //⑥用户可以调用isDogEmpty方法，检查队列里，是否还有Dog类的实例；
    //⑦用户可以调用isCatEmpty方法，检查队列里，是否还有Cat类的示例；

    static class Pet {
        private String type;

        public Pet(String type) {
            this.type = type;
        }

        public String getType() {
            return this.type;
        }
    }

    class Dog extends Pet {
        public Dog() {
            super("Dog");
        }
    }

    class Cat extends Pet {
        public Cat() {
            super("Cat");
        }
    }


    static class PetEntry {
        private Pet pet;
        private long count;

        public PetEntry(Pet pet, long count) {
            this.pet = pet;
            this.count = count;
        }

        public Pet getPet() {
            return this.pet;
        }

        public long getCount() {
            return this.count;
        }

        public String getEntryPettype() {
            return this.pet.getType();
        }
    }

    static class CatDogQueue {
        private Queue<PetEntry> catQueue = new LinkedList<PetEntry>();
        private Queue<PetEntry> dogQueue = new LinkedList<PetEntry>();
        private long count;

        public CatDogQueue() {
            Queue<PetEntry> catQueue = new LinkedList<PetEntry>();
            Queue<PetEntry> dogQueue = new LinkedList<PetEntry>();
            long count = 0;
        }

        /**
         * 队列存的其实是 pet和count版本号  并不是只有pet对象  版本号小的就是先存进去的
         *
         * @param pet
         */
        public void add(Pet pet) {
            if (pet.getType().equals("Dog")) {
                this.dogQueue.add(new PetEntry(pet, this.count++));
            } else if (pet.getType().equals("Cat")) {
                this.catQueue.add(new PetEntry(pet, this.count++));
            } else throw new RuntimeException("error! input not dog or cat!");
        }

        /**
         * 队列 先进先出
         * peek队列先进入的元素，看哪个比较小，小的先出
         *
         * @return
         */
        public Pet popAll() {
            if (!catQueue.isEmpty() && !dogQueue.isEmpty()) {
                if (catQueue.peek().getCount() < dogQueue.peek().getCount()) {
                    return catQueue.poll().getPet();
                } else {
                    return dogQueue.poll().getPet();
                }
            } else if (!catQueue.isEmpty()) {
                return catQueue.poll().getPet();
            } else if (!dogQueue.isEmpty()) {
                return dogQueue.poll().getPet();
            } else {
                throw new RuntimeException("err! queue is empty");
            }
        }

        public Pet popDog() {
            if (!dogQueue.isEmpty()) {
                return dogQueue.poll().getPet();
            } else throw new RuntimeException("DogQueue is Empty");
        }

        public Pet popCat() {
            if (!catQueue.isEmpty()) {
                return catQueue.poll().getPet();
            } else throw new RuntimeException("DogQueue is Empty");
        }

        public boolean isEmpty() {
            return dogQueue.isEmpty() && catQueue.isEmpty();
        }

        public boolean dogqueueisEmpty() {
            return dogQueue.isEmpty();
        }

        public boolean catqueueisEmpty() {
            return catQueue.isEmpty();
        }
    }

    public static void main(String[] args) {
        CatDogQueue cdq = new CatDogQueue();
        //add
        System.out.println("add four Pets");
        cdq.add(new Pet("Dog"));
        cdq.add(new Pet("Cat"));
        cdq.add(new Pet("Cat"));
        cdq.add(new Pet("Dog"));
        cdq.add(new Pet("Cat"));
        System.out.println("--------------------------");
        //popAll
        System.out.println("popAll  2");
        System.out.println(cdq.popAll().getType());
        System.out.println(cdq.popAll().getType());
        System.out.println("--------------------------");
        //popCat
        System.out.println("popCat  1");
        System.out.println(cdq.popCat().getType());
        System.out.println("--------------------------");
        //popDog
        System.out.println("popDog  1");
        System.out.println(cdq.popDog().getType());
        System.out.println("--------------------------");
        System.out.println("dogQueque is empty?");
        System.out.println(cdq.dogqueueisEmpty());
        System.out.println("--------------------------");
        System.out.println("catQueque is empty?");
        System.out.println(cdq.catqueueisEmpty());
        System.out.println("--------------------------");
        System.out.println(" is  all Empty");
        System.out.println(cdq.isEmpty());
        System.out.println("--------------------------");
        cdq.popCat();
        System.out.println(" now  is  all Empty");
        System.out.println(cdq.isEmpty());
        System.out.println("--------------------------");
    }
}
