//给定一个字符串 s ，找到 它的第一个不重复的字符，并返回它的索引 。如果不存在，则返回 -1 。 
//
// 
//
// 示例 1： 
//
// 
//输入: s = "leetcode"
//输出: 0
// 
//
// 示例 2: 
//
// 
//输入: s = "loveleetcode"
//输出: 2
// 
//
// 示例 3: 
//
// 
//输入: s = "aabb"
//输出: -1
// 
//
// 
//
// 提示: 
//
// 
// 1 <= s.length <= 10⁵ 
// s 只包含小写字母 
// 
//
// Related Topics 队列 哈希表 字符串 计数 👍 687 👎 0

//字符串中的第一个唯一字符
package leetcode.editor.cn;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

class P387FirstUniqueCharacterInAString {
    public static void main(String[] args) {
        Solution solution = new P387FirstUniqueCharacterInAString().new Solution();
        int num = solution.firstUniqChar("leetcode");
        System.out.println(num);
    }

    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        public int firstUniqChar(String s) {
            return fun3(s);
        }

        /**
         * map存储索引
         */
        private int fun3(String s) {
            HashMap<Character, Integer> map = new HashMap<>();
            char[] chars = s.toCharArray();
            for (int i = 0; i < chars.length; i++) {
                map.put(chars[i], map.containsKey(chars[i]) ? -1 : i);
            }
            int first = s.length();
            for (Map.Entry<Character, Integer> entry : map.entrySet()) {
                Integer position = entry.getValue();
                if (position != -1 && position < first) {
                    first = position;
                }
            }
            if (first == s.length()) {
                return -1;
            }
            return first;
        }

        private int fun2(String s) {
            HashMap<Character, Integer> map = new HashMap<>();
            char[] chars = s.toCharArray();
            for (char c : chars) {
                map.put(c, map.getOrDefault(c, 0) + 1);
            }
            for (int i = 0; i < chars.length; i++) {
                if (map.get(chars[i]) == 1) {
                    return i;
                }
            }
            return -1;
        }

        /**
         * 数组存储出现次数
         */
        private int fun1(String s) {
            int[] nums = new int[26];
            char[] chars = s.toCharArray();
            for (char c : chars) {
                nums[c - 'a']++;
            }
            for (int i = 0; i < chars.length; i++) {
                char aChar = chars[i];
                if (nums[chars[i] - 'a'] == 1) {
                    return i;
                }
            }
            return -1;
        }
    }
//leetcode submit region end(Prohibit modification and deletion)
}