//给定一个仅包含数字 2-9 的字符串，返回所有它能表示的字母组合。答案可以按 任意顺序 返回。 
//
// 给出数字到字母的映射如下（与电话按键相同）。注意 1 不对应任何字母。 
//
// 
//
// 
//
// 示例 1： 
//
// 
//输入：digits = "23"
//输出：["ad","ae","af","bd","be","bf","cd","ce","cf"]
// 
//
// 示例 2： 
//
// 
//输入：digits = ""
//输出：[]
// 
//
// 示例 3： 
//
// 
//输入：digits = "2"
//输出：["a","b","c"]
// 
//
// 
//
// 提示： 
//
// 
// 0 <= digits.length <= 4 
// digits[i] 是范围 ['2', '9'] 的一个数字。 
// 
//
// Related Topics 哈希表 字符串 回溯 👍 2557 👎 0

//电话号码的字母组合
package leetcode.editor.cn;

import java.util.ArrayList;
import java.util.List;

class P17LetterCombinationsOfAPhoneNumber {
    public static void main(String[] args) {
        Solution solution = new P17LetterCombinationsOfAPhoneNumber().new Solution();
        // TO TEST
    }

    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        char[][] phone = {     //按键   //顺序
                {'a', 'b', 'c'}, // 2    0
                {'d', 'e', 'f'}, // 3    1
                {'g', 'h', 'i'}, // 4    2
                {'j', 'k', 'l'}, // 5    3
                {'m', 'n', 'o'}, // 6
                {'p', 'q', 'r', 's'}, // 7
                {'t', 'u', 'v'},   // 8
                {'w', 'x', 'y', 'z'}, // 9
        };

        public List<String> letterCombinations(String digits) {
            List<String> ans = new ArrayList<>();
            if (digits == null || digits.length() == 0) {
                return ans;
            }
            char[] str = digits.toCharArray();//按键
            char[] path = new char[digits.length()];
            process(str, 0, path, ans);
            return ans;
        }

        private void process(char[] str, int index, char[] path, List<String> ans) {
            if (index == str.length) {
                ans.add(String.valueOf(path));
            } else {
                char[] cands = phone[str[index] - '2'];
                for (char cur : cands) {
                    path[index] = cur;
                    process(str, index + 1, path, ans);
                }
            }
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}