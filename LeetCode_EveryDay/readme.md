leetcode plugin
1. 配置 Cdoe File Name
``` 
   P[$!{question.frontendQuestionId}]${question.title} 
```

2. 配置代码模板 Code Template
```
${question.content}
//${question.title}
package leetcode.editor.cn;
public class P${question.frontendQuestionId}$!velocityTool.camelCaseName(${question.titleSlug}){
    public static void main(String[] args) {
    Solution solution = new P$!{question.frontendQuestionId}$!velocityTool.camelCaseName(${question.titleSlug})().new Solution();
    // TO TEST
    }
    ${question.code}
}
```
