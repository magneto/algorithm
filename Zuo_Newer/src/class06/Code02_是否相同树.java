package class06;

import utils.TreeNode;

/**
 * @Description: 是否相同树
 * @Author: WangWenpeng
 */
public class Code02_是否相同树 {

    public static boolean isSameTree(TreeNode p, TreeNode q) {
        //有一个为空 一个不为空   异或操作
        if (p == null ^ q == null) {
            return false;
        }
        if (p == null && q == null) {
            return true;
        }
        // 都不为空
        return p.val == q.val && isSameTree(p.left, q.left) && isSameTree(p.right, q.right);
    }
}
