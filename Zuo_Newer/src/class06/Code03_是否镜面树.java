package class06;

import utils.TreeNode;

/**
 * @Description: 是否镜面树
 * @Author: WangWenpeng
 */
public class Code03_是否镜面树 {

    public static boolean isSymmetric(TreeNode root) {
        return isMirror(root, root);
    }

    public static boolean isMirror(TreeNode h1, TreeNode h2) {
        //异或  有一个不为空
        if (h1 == null ^ h2 == null) {
            return false;
        }
        //两个都为空 相等
        if (h1 == null && h2 == null) {
            return true;
        }
        return h1.val == h2.val && isMirror(h1.left, h2.right) && isMirror(h1.right, h2.left);
    }
}
