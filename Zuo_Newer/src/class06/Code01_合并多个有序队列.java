package class06;

import utils.ListNode;

import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * @Description: 合并多个有序队列
 * @Author: WangWenpeng
 */
public class Code01_合并多个有序队列 {

    //比较器 节点大小的比较方法
    public static class NodeComparator implements Comparator<ListNode> {
        @Override
        public int compare(ListNode o1, ListNode o2) {
            return o1.value - o2.value;
        }
    }

    //根据给定的头结点列表返回排序后的唯一头节点
    public static ListNode mergeKLists(ListNode[] lists) {
        if (lists == null) {
            return null;
        }
        //java的小根堆  使用 优先级队列实现； 头节点都放进去
        PriorityQueue<ListNode> heap = new PriorityQueue<>(new NodeComparator());
        for (int i = 0; i < lists.length; i++) {
            if (lists[i] != null) {
                heap.add(lists[i]);
            }
        }
        if (heap.isEmpty()) {
            return null;
        }

        //把处理头节点和其他节点的分开了 容易理解
        ListNode head = heap.poll(); //head是最小的，抓住头节点
        ListNode pre = head;
        if (pre.next != null) {
            heap.add(pre.next);
        }
        //弹一个往里塞一个
        while (!heap.isEmpty()) {
            ListNode cur = heap.poll();
            pre.next = cur;
            pre = cur;
            if (cur.next != null) {
                heap.add(cur.next);
            }
        }
        return head;
    }
}
