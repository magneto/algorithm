package class06;

import utils.TreeNode;

/**
 * @Description: 树最大深度
 * @Author: WangWenpeng
 */
public class Code04_树最大深度 {

    // 以root为头的树，最大高度是多少返回！
    public static int maxDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        return Math.max(maxDepth(root.left), maxDepth(root.right)) + 1;
    }
}
