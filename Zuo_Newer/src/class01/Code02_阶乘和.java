package class01;

/**
 * @Description 1-n的阶乘之和
 * @Author WangWenpeng
 * @Date 8:40 2021/5/9
 * @Param
 */
public class Code02_阶乘和 {

	/**
	 * @Description 低效方法
	 * @Author WangWenpeng
	 * @Date 8:41 2021/5/9
	 * @Param [N]
	 */
	public static long f1(int N) {
		long ans = 0;
		for (int i = 1; i <= N; i++) {
			ans += factorial(i);
		}
		return ans;
	}

	public static long factorial(int N) {
		long ans = 1;
		for (int i = 1; i <= N; i++) {
			ans *= i;
		}
		return ans;
	}

	/**
	 * @Description 高效
	 * @Author WangWenpeng
	 * @Date 8:42 2021/5/9
	 * @Param [N]
	 */
	public static long f2(int N) {
		long ans = 0;
		long cur = 1;
		for (int i = 1; i <= N; i++) {
			cur = cur * i;
			ans += cur;
		}
		return ans;
	}

	public static void main(String[] args) {
		int N = 10;
		System.out.println(f1(N));
		System.out.println(f2(N));
	}
}
