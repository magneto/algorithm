package class01;

/**
 * @ClassName Code01_PrintB
 * @Description: 打印int的2进制   int的长度32位
 * @Author: WangWenpeng
 * @date: 7:53 2021/5/9
 * @Version 1.0
 */
public class Code01_打印二进制 {
    /**
     * @Description int长度为32位 每一位和当前位为1的进行按位与预算。
     * @Author WangWenpeng
     * @Date 8:35 2021/5/9
     * @Param [num]
     */
    public static void print(int num) {
        //  <<  无符号左移一位 左移都是无符号的 右边拿0补  左移相当于乘2
        //  >> 带符号右移
        //  >>> 不带符号右移
        //  &运算
        for (int i = 31; i >= 0; i--) {
            System.out.print((num & (1 << i)) == 0 ? "0" : "1");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        print(31);
        print(Integer.MIN_VALUE);
    }
}
