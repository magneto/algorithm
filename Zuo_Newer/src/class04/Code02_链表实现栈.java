package class04;

import utils.ListNode;

public class Code02_链表实现栈 {
    public static void main(String[] args) {

    }

    public static class MyStack {
        private ListNode head;
        private int size;

        private boolean isEmpty() {
            return size == 0;
        }

        private int getSize() {
            return size;
        }

        private void push(int value) {
            ListNode listNode = new ListNode(value);
            if (size == 0) {
                head = listNode;
            } else {
                head.next = listNode;
                head = listNode;
            }
            size++;
        }

        private ListNode pop() {
            if (size == 0) {
                return null;
            } else {
                ListNode ans = head;
                head = head.next;
                size--;
                return ans;
            }
        }

        private ListNode peek() {
            return size == 0 ? null : head;
        }
    }
}
