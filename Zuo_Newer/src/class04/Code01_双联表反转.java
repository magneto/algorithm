package class04;

import utils.DoubleNode;
import utils.LinkListUtil;

public class Code01_双联表反转 {
    public static void main(String[] args) {
        DoubleNode doubleNode = LinkListUtil.generateDoubleRandomList(10, 10, false);
        LinkListUtil.printDoubleLinkList(doubleNode);
        DoubleNode reverseNode = reverseDoubleLinkedList(doubleNode);
        LinkListUtil.printDoubleLinkList(reverseNode);
    }

    /**
     * 双链表的反转
     */
    public static DoubleNode reverseDoubleLinkedList(DoubleNode doubleNode) {
        DoubleNode pre = null;
        DoubleNode next = null;
        DoubleNode head = LinkListUtil.copyNode(doubleNode);
        while (head != null) {
            next = head.next;
            head.last = next;
            head.next = pre;
            pre = head;
            head = next;
        }
        return pre;
    }

}
