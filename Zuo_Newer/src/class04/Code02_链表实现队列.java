package class04;

import utils.ListNode;

public class Code02_链表实现队列 {


    public static void main(String[] args) {

    }

    /**
     * 链表实现队列
     */
    public static class MyQueue {
        private ListNode head;
        private ListNode tail;
        private int size;

        private boolean isEmpty() {
            return size == 0;
        }

        private int getSize() {
            return size;
        }

        private void offer(int value) {
            ListNode listNode = new ListNode(value);
            if (size == 0) {
                head = listNode;
                tail = listNode;
            } else {
                tail.next = listNode;
                tail = listNode;
            }
            size++;
        }

        private ListNode poll() {
            if (size == 0) {
                return null;
            } else {
                ListNode ans = head;
                head = head.next;
                size--;
                return ans;
            }
        }

        private ListNode peek() {
            if (size == 0) {
                return null;
            } else {
                return head;
            }
        }

    }

}

