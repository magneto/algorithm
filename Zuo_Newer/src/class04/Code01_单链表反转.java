package class04;

import utils.ArrayUtil;
import utils.LinkListUtil;
import utils.ListNode;

public class Code01_单链表反转 {

    /**
     * 链表的反转没问题
     * 自测代码会导致链表head指向错误
     * 此例只打印出链表 不在执行代码检测
     */
    public static void main(String[] args) {
        int length = 10;
        int maxValue = 100;
        int testTime = 100000;
        System.out.println("test begin!");
        for (int i = 0; i < testTime; i++) {
            ListNode head = LinkListUtil.generateRandomList(length, maxValue, false);
            // 打印会导致头节点偏移 引用传递 所以要复制一个引用往下传
            LinkListUtil.printLinkList(head);
            ListNode reverseHead = reverseLinkList(head);
            LinkListUtil.printLinkList(reverseHead);
            System.out.println("-----------------------------");
            if (!checkReverse(LinkListUtil.copyNode(head), LinkListUtil.copyNode(reverseHead), length)) {
                System.out.println("fuck!");
            }
        }
    }

    /**
     * 是否链表反转的校验
     */
    private static boolean checkReverse(ListNode head, ListNode reverseHead, int length) {
        LinkListUtil.printLinkList(head);
        int[] headArray = LinkListUtil.linkListToArray(head, length);
        LinkListUtil.printLinkList(reverseHead);
        int[] reverseArray = LinkListUtil.linkListToArray(reverseHead, length);
        return ArrayUtil.isRevere(headArray, reverseArray);
    }

    /**
     * 单链表的反转
     */
    public static ListNode reverseLinkList(ListNode head) {
        ListNode pre = null;
        ListNode next = null;
        ListNode copyHead = LinkListUtil.copyNode(head);
        while (copyHead != null) {
            next = copyHead.next;
            copyHead.next = pre;
            pre = copyHead;
            copyHead = next;
        }
        return pre;
    }
}
