package class03;

import utils.ArrayUtil;

import java.util.Arrays;

public class Code01_二分查找 {

    /**
     * 出现了暴力和二分不一致的结果 数组中出现了两次相同的数
     * 暴力永远找的都是第一个数的位置
     * 二分可能找的是第二个或者以后出现的位置
     *
     *  位运算优先级低于加减运算  要加括号先保证位运算
     *  https://blog.csdn.net/sadjladjlas/article/details/51192106
     */

    /**
     * 有序数组中查找给定的num存不存在 二分法查找
     */
    public static int binarySearch(int[] arrs, int num) {
        if (arrs == null || arrs.length == 0) {
            return -1;
        }

        int L = 0;
        int R = arrs.length - 1;
        while (L <= R) {
            int middle = L + ((R - L) >> 1);//右移1位才是 除以2   右移2等于除以4
            //int middle = (L + R) / 2;
            if (arrs[middle] == num) {
                return middle;
            } else if (arrs[middle] < num) {
                L = middle + 1;
            } else {
                R = middle - 1;
            }
        }
        return -1;
    }

    /**
     * 暴力查询数组中存在num的位置
     */
    public static int violenceSearch(int[] arrs, int num) {
        for (int i = 0; i < arrs.length; i++) {
            if (arrs[i] == num) {
                return i;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        int times = 10;
        int num = 10;
        for (int i = 0; i < times; i++) {
            int[] arrs = ArrayUtil.generateRandomArray(10, 50, false, false);
            Arrays.sort(arrs);
            int rst1 = binarySearch(arrs, num);
            int rst2 = violenceSearch(arrs, num);
            if (rst1 != rst2) {
                System.out.println("fucking fucked");
                ArrayUtil.printArray(arrs);
                System.out.println("length:" + arrs.length);
                System.out.println("num:" + num);
                System.out.println("rst1:" + rst1);
                System.out.println("rst2:" + rst2);
            }
        }
        System.out.println("game over");
    }
}
