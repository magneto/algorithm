package class03;

import utils.ArrayUtil;

import java.util.Arrays;

public class Code03_二分查找小于最右 {
    public static void main(String[] args) {
        int times = 100;
        for (int i = 0; i < times; i++) {
            int[] arr = ArrayUtil.generateRandomArray(1000, 100, false, true);
            Arrays.sort(arr);
            int num = arr[10];
            int ans1 = findNearRight(arr, num);
            int ans2 = force(arr, num);
            if (ans1 != ans2) {
                System.out.println("fuck error");
                ArrayUtil.printArray(arr);
                System.out.println(num);
                System.out.println("ans1:" + ans1);
                System.out.println("ans2:" + ans2);
            }
        }
        System.out.println("over");
    }

    /**
     * 二分 有序数组中查找小于等于num的最右位置
     */
    public static int findNearRight(int[] arr, int num) {
        if (arr == null || arr.length == 0) {
            return -1;
        }

        int L = 0;
        int R = arr.length - 1;
        int ans = -1;
        while (L <= R) {
            int middle = L + ((R - L) >> 1);
            if (arr[middle] <= num) {
                ans = middle;
                L = middle + 1;
            } else {
                R = middle - 1;
            }
        }
        return ans;
    }

    /**
     * 暴力查找小于等于左右
     */
    public static int force(int[] arr, int num) {
        for (int i = arr.length - 1; i >= 0; i--) {
            if (arr[i] <= num) {
                return i;
            }
        }
        return -1;
    }
}
