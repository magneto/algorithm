package class02;

public class Code02_概率转平方 {

    /**
     * 返回[0,1)的一个小数
     * 任意的x，x属于[0,1)，[0,x)范围上的数出现概率由原来的x调整成x平方      两次取较大的值
     */
    public static double xToXPower2() {
        return Math.max(Math.random(), Math.random());
    }

    public static void main(String[] args) {
        int count = 0;
        int testTimes = 10000;
        double x = 0.17;
        for (int i = 0; i < testTimes; i++) {
            if (xToXPower2() < x) {
                count++;
            }
        }
        System.out.println((double) count / (double) testTimes);// 0.17的平方的概率
        System.out.println((double) 1 - Math.pow((double) 1 - x, 2));
    }
}
