package utils;

/**
 * 双链表的节点
 */
public class DoubleNode {
    public int value;
    public DoubleNode next;
    public DoubleNode last;

    public DoubleNode(int value) {
        this.value = value;
    }

    public DoubleNode(int value, DoubleNode next, DoubleNode last) {
        this.value = value;
        this.next = next;
        this.last = last;
    }
}
