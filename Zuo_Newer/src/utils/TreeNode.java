package utils;

/**
 * @ClassName TreeNode
 * @Description:
 * @Author: WangWenpeng
 * @date: 16:52 2022/4/29
 * @Version 1.0
 */
public class TreeNode {
    public int val;
    public TreeNode left;
    public TreeNode right;

    public TreeNode(int val) {
        this.val = val;
    }

    public TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}
