//给你一个整数数组 nums 和一个整数 k ，请你统计并返回 该数组中和为 k 的连续子数组的个数 。
//
//
//
// 示例 1：
//
//
//输入：nums = [1,1,1], k = 2
//输出：2
//
//
// 示例 2：
//
//
//输入：nums = [1,2,3], k = 3
//输出：2
//
//
//
//
// 提示：
//
//
// 1 <= nums.length <= 2 * 10⁴
// -1000 <= nums[i] <= 1000
// -10⁷ <= k <= 10⁷
//
// Related Topics 数组 哈希表 前缀和 👍 1600 👎 0

package leetcode.editor.cn;

import java.util.HashMap;

class 和为K的子数组 {
    public static void main(String[] args) {
        Solution solution = new 和为K的子数组().new Solution();
        // TO TEST
    }

    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        public int subarraySum(int[] nums, int k) {
            if (nums == null || nums.length == 0) {
                return 0;
            }
            HashMap<Integer, Integer> preSumTimesMap = new HashMap<>();
            preSumTimesMap.put(0, 1);
            int all = 0; // 0..i
            int ans = 0;
            for (int i = 0; i < nums.length; i++) {
                all += nums[i]; // 0....i 整体的前缀和
                if (preSumTimesMap.containsKey(all - k)) {
                    ans += preSumTimesMap.get(all - k);
                }
                if (!preSumTimesMap.containsKey(all)) {
                    preSumTimesMap.put(all, 1);
                } else {
                    preSumTimesMap.put(all, preSumTimesMap.get(all) + 1);
                }
            }
            return ans;
        }
    }

//leetcode submit region end(Prohibit modification and deletion)

}
