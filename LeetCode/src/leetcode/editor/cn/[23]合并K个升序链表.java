//给你一个链表数组，每个链表都已经按升序排列。
//
// 请你将所有链表合并到一个升序链表中，返回合并后的链表。
//
//
//
// 示例 1：
//
// 输入：lists = [[1,4,5],[1,3,4],[2,6]]
//输出：[1,1,2,3,4,4,5,6]
//解释：链表数组如下：
//[
//  1->4->5,
//  1->3->4,
//  2->6
//]
//将它们合并到一个有序链表中得到。
//1->1->2->3->4->4->5->6
//
//
// 示例 2：
//
// 输入：lists = []
//输出：[]
//
//
// 示例 3：
//
// 输入：lists = [[]]
//输出：[]
//
//
//
//
// 提示：
//
//
// k == lists.length
// 0 <= k <= 10^4
// 0 <= lists[i].length <= 500
// -10^4 <= lists[i][j] <= 10^4
// lists[i] 按 升序 排列
// lists[i].length 的总和不超过 10^4
//
// Related Topics 链表 分治 堆（优先队列） 归并排序 👍 2030 👎 0

package leetcode.editor.cn;

import java.util.*;

class 合并K个升序链表 {
    public static void main(String[] args) {
        Solution solution = new 合并K个升序链表().new Solution();
        //ListNode listNode1 = new ListNode(1, new ListNode(4, new ListNode(5, null)));
        //ListNode listNode2 = new ListNode(1, new ListNode(3, new ListNode(4, null)));
        //ListNode listNode3 = new ListNode(2, new ListNode(6, null));

        //ListNode listNode1 = new ListNode(-2, new ListNode(-1, new ListNode(-1, new ListNode(-1, null))));

        //[[-1,1],[-3,1,4],[-2,-1,0,2]]
        ListNode listNode1 = new ListNode(-1, new ListNode(1, null));
        ListNode listNode2 = new ListNode(-3, new ListNode(1, new ListNode(4, null)));
        ListNode listNode3 = new ListNode(-2, new ListNode(-1, new ListNode(0, new ListNode(2, null))));
        ListNode[] listNodes = {listNode1, listNode2, listNode3};
        ListNode node = solution.mergeKLists(listNodes);
        while (node != null) {
            System.out.print(node.val + " ");
            node = node.next;
        }
        System.out.println();
    }

    public static class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }
    //leetcode submit region begin(Prohibit modification and deletion)

    //左老师的思路只放头结点
    //这里的思路是全放进去之后往外拿
    class Solution {
        public class ListNodeCompare implements Comparator<ListNode> {
            @Override
            public int compare(ListNode o1, ListNode o2) {
                return o1.val - o2.val;
            }
        }

        //输入：lists = [[1,4,5],[1,3,4],[2,6]]
        public ListNode mergeKLists(ListNode[] lists) {
            if (lists == null) {
                return null;
            }
            PriorityQueue<ListNode> heap = new PriorityQueue<>(new ListNodeCompare());
            for (ListNode list : lists) {
                if (list != null) {
                    heap.add(list);
                    while (list.next != null) {
                        heap.add(list.next);
                        list.next = list.next.next;
                    }
                }
            }
            if (heap.isEmpty()) {
                return null;
            }
            ListNode head = heap.poll();
            ListNode pre = head;
            while (!heap.isEmpty()) {
                ListNode cur = heap.poll();
                cur.next = null;
                pre.next = cur;
                pre = cur;
            }
            return head;
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}
