//给你两个 非空 的链表，表示两个非负的整数。它们每位数字都是按照 逆序 的方式存储的，并且每个节点只能存储 一位 数字。
//
// 请你将两个数相加，并以相同形式返回一个表示和的链表。
//
// 你可以假设除了数字 0 之外，这两个数都不会以 0 开头。
//
//
//
// 示例 1：
//
//
//输入：l1 = [2,4,3], l2 = [5,6,4]
//输出：[7,0,8]
//解释：342 + 465 = 807.
//
//
// 示例 2：
//
//
//输入：l1 = [0], l2 = [0]
//输出：[0]
//
//
// 示例 3：
//
//
//输入：l1 = [9,9,9,9,9,9,9], l2 = [9,9,9,9]
//输出：[8,9,9,9,0,0,0,1]
//
//
//
//
// 提示：
//
//
// 每个链表中的节点数在范围 [1, 100] 内
// 0 <= Node.val <= 9
// 题目数据保证列表表示的数字不含前导零
//
// Related Topics 递归 链表 数学
// 👍 5489 👎 0


package leetcode.editor.cn;

class 两数相加 {
    public static void main(String[] args) {
        //ListNode l1 = new ListNode(2, new ListNode(4, new ListNode(3, new ListNode(9, null))));
        //ListNode l2 = new ListNode(5, new ListNode(6, new ListNode(9, null)));
        ListNode l1 = new ListNode(2, new ListNode(4, new ListNode(3, null)));
        ListNode l2 = new ListNode(5, new ListNode(6, new ListNode(4, null)));
        Solution solution = new 两数相加().new Solution();
        ListNode listNode = solution.addTwoNumbers(l1, l2);
        while (listNode != null) {
            System.out.print(listNode.val + " ");
            listNode = listNode.next;
        }
    }

    public static class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }

    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        public ListNode addTwoNumbers(ListNode l1, ListNode l2) {

            ListNode longList = nodeLength(l1) > nodeLength(l2) ? l1 : l2;
            ListNode shortList = l1 == longList ? l2 : l1;

            ListNode curL = longList;
            ListNode curS = shortList;
            ListNode last = curL;
            int carry = 0;
            int curNum = 0;
            while (curS != null) {
                curNum = curL.val + curS.val + carry;
                curL.val = (curNum % 10);
                carry = curNum / 10;
                last = curL;
                curL = curL.next;
                curS = curS.next;
            }
            while (curL != null) {
                curNum = curL.val + carry;
                curL.val = (curNum % 10);
                carry = curNum / 10;
                last = curL;
                curL = curL.next;
            }
            if (carry != 0) {
                last.next = new ListNode(1);
            }
            return longList;
        }

        //链表长度
        public int nodeLength(ListNode head) {
            int length = 0;
            while (head != null) {
                length++;
                head = head.next;
            }
            return length;
        }

    }
//leetcode submit region end(Prohibit modification and deletion)

}
