//编写一个程序，通过填充空格来解决数独问题。
//
// 数独的解法需 遵循如下规则：
//
//
// 数字 1-9 在每一行只能出现一次。
// 数字 1-9 在每一列只能出现一次。
// 数字 1-9 在每一个以粗实线分隔的 3x3 宫内只能出现一次。（请参考示例图）
//
//
// 数独部分空格内已填入了数字，空白格用 '.' 表示。
//
//
//
//
//
//
// 示例 1：
//
//
//输入：board = [["5","3",".",".","7",".",".",".","."],["6",".",".","1","9","5",".
//",".","."],[".","9","8",".",".",".",".","6","."],["8",".",".",".","6",".",".",".
//","3"],["4",".",".","8",".","3",".",".","1"],["7",".",".",".","2",".",".",".","6
//"],[".","6",".",".",".",".","2","8","."],[".",".",".","4","1","9",".",".","5"],[
//".",".",".",".","8",".",".","7","9"]]
//输出：[["5","3","4","6","7","8","9","1","2"],["6","7","2","1","9","5","3","4","8
//"],["1","9","8","3","4","2","5","6","7"],["8","5","9","7","6","1","4","2","3"],[
//"4","2","6","8","5","3","7","9","1"],["7","1","3","9","2","4","8","5","6"],["9",
//"6","1","5","3","7","2","8","4"],["2","8","7","4","1","9","6","3","5"],["3","4",
//"5","2","8","6","1","7","9"]]
//解释：输入的数独如上图所示，唯一有效的解决方案如下所示：
//
//
//
//
//
//
// 提示：
//
//
// board.length == 9
// board[i].length == 9
// board[i][j] 是一位数字或者 '.'
// 题目数据 保证 输入数独仅有一个解
//
//
//
//
// Related Topics 数组 回溯 矩阵 👍 1310 👎 0

package leetcode.editor.cn;

class 解数独 {
    public static void main(String[] args) {
        Solution solution = new 解数独().new Solution();
        char[][] board = {  {'5', '3', '.',     '.', '7', '.',      '.', '.', '.'},
                            {'6', '.', '.',     '1', '9', '5',      '.', '.', '.'},
                            {'.', '9', '8',     '.', '.', '.',      '.', '6', '.'},

                            {'8', '.', '.',     '.', '6', '.',      '.', '.', '3'},
                            {'4', '.', '.',     '8', '.', '3',      '.', '.', '1'},
                            {'7', '.', '.',     '.', '2', '.',      '.', '.', '6'},

                            {'.', '6', '.',     '.', '.', '.',      '2', '8', '.'},
                            {'.', '.', '.',     '4', '1', '9',      '.', '.', '5'},
                            {'.', '.', '.',     '.', '8', '.',      '.', '7', '9'}};
         solution.solveSudoku(board);
    }

    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        public void solveSudoku(char[][] board) {
            boolean[][] row = new boolean[9][10];
            boolean[][] col = new boolean[9][10];
            boolean[][] bucket = new boolean[9][10];
            initMaps(board, row, col, bucket);
            process(board, 0, 0, row, col, bucket);
        }

        public void initMaps(char[][] board, boolean[][] row, boolean[][] col, boolean[][] bucket) {
            for (int i = 0; i < 9; i++) {
                for (int j = 0; j < 9; j++) {
                    int bid = 3 * (i / 3) + (j / 3);
                    if (board[i][j] != '.') {
                        int num = board[i][j] - '0';
                        row[i][num] = true;
                        col[j][num] = true;
                        bucket[bid][num] = true;
                    }
                }
            }
        }

        //  当前来到(i,j)这个位置，如果已经有数字，跳到下一个位置上
        //                      如果没有数字，尝试1~9，不能和row、col、bucket冲突
        public boolean process(char[][] board, int i, int j, boolean[][] row, boolean[][] col, boolean[][] bucket) {
            if (i == 9) {
                return true;
            }
            // 当离开(i，j)，应该去哪？(nexti, nextj)
            int nexti = j != 8 ? i : i + 1;
            int nextj = j != 8 ? j + 1 : 0;
            if (board[i][j] != '.') {
                return process(board, nexti, nextj, row, col, bucket);
            } else {
                // 可以尝试1~9
                int bid = 3 * (i / 3) + (j / 3);
                for (int num = 1; num <= 9; num++) { // 尝试每一个数字1~9
                    if ((!row[i][num]) && (!col[j][num]) && (!bucket[bid][num])) {
                        // 可以尝试num
                        row[i][num] = true;
                        col[j][num] = true;
                        bucket[bid][num] = true;
                        board[i][j] = (char) (num + '0');
                        if (process(board, nexti, nextj, row, col, bucket)) {
                            return true;
                        }
                        row[i][num] = false;
                        col[j][num] = false;
                        bucket[bid][num] = false;
                        board[i][j] = '.';
                    }
                }
                return false;
            }
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}
