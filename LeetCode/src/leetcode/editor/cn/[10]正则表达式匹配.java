//给你一个字符串 s 和一个字符规律 p，请你来实现一个支持 '.' 和 '*' 的正则表达式匹配。
//
//
// '.' 匹配任意单个字符
// '*' 匹配零个或多个前面的那一个元素
//
//
// 所谓匹配，是要涵盖 整个 字符串 s的，而不是部分字符串。
//
//
// 示例 1：
//
//
//输入：s = "aa", p = "a"
//输出：false
//解释："a" 无法匹配 "aa" 整个字符串。
//
//
// 示例 2:
//
//
//输入：s = "aa", p = "a*"
//输出：true
//解释：因为 '*' 代表可以匹配零个或多个前面的那一个元素, 在这里前面的元素就是 'a'。因此，字符串 "aa" 可被视为 'a' 重复了一次。
//
//
// 示例 3：
//
//
//输入：s = "ab", p = ".*"
//输出：true
//解释：".*" 表示可匹配零个或多个（'*'）任意字符（'.'）。
//
//
//
//
// 提示：
//
//
// 1 <= s.length <= 20
// 1 <= p.length <= 30
// s 只包含从 a-z 的小写字母。
// p 只包含从 a-z 的小写字母，以及字符 . 和 *。
// 保证每次出现字符 * 时，前面都匹配到有效的字符
//
// Related Topics 递归 字符串 动态规划 👍 3048 👎 0

package leetcode.editor.cn;

class 正则表达式匹配 {
    public static void main(String[] args) {
        Solution solution = new 正则表达式匹配().new Solution();
        // TO TEST
    }

    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        public boolean isMatch(String str, String exp) {
            if (str == null || exp == null) {
                return false;
            }

            char[] s = str.toCharArray();
            char[] e = exp.toCharArray();
            return isValid(s, e) && process(s, e, 0, 0);
        }

        public boolean process(char[] s, char[] e, int si, int ei) {
            if (ei == e.length) { // exp 没了 str？
                return si == s.length;
            }
            // exp[ei]还有字符
            // ei + 1位置的字符，不是*
            if (ei + 1 == e.length || e[ei + 1] != '*') {
                // ei + 1 不是*
                // str[si] 必须和 exp[ei] 能配上！
                return si != s.length && (e[ei] == s[si] || e[ei] == '.') && process(s, e, si + 1, ei + 1);
            }
            // exp[ei]还有字符
            // ei + 1位置的字符，是*!
            while (si != s.length && (e[ei] == s[si] || e[ei] == '.')) {
                if (process(s, e, si, ei + 2)) {
                    return true;
                }
                si++;
            }
            return process(s, e, si, ei + 2);
        }

        //参数校验
        private boolean isValid(char[] s, char[] e) {
            // s中不能有'.' or '*'
            for (int i = 0; i < s.length; i++) {
                if (s[i] == '*' || s[i] == '.') {
                    return false;
                }
            }
            // 开头的e[0]不能是'*'，没有相邻的'*'
            for (int i = 0; i < e.length; i++) {
                if (e[i] == '*' && (i == 0 || e[i - 1] == '*')) {
                    return false;
                }
            }
            return true;
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}
