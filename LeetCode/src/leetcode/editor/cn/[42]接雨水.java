//给定 n 个非负整数表示每个宽度为 1 的柱子的高度图，计算按此排列的柱子，下雨之后能接多少雨水。
//
//
//
// 示例 1：
//
//
//
//
//输入：height = [0,1,0,2,1,0,1,3,2,1,2,1]
//输出：6
//解释：上面是由数组 [0,1,0,2,1,0,1,3,2,1,2,1] 表示的高度图，在这种情况下，可以接 6 个单位的雨水（蓝色部分表示雨水）。
//
//
// 示例 2：
//
//
//输入：height = [4,2,0,3,2,5]
//输出：9
//
//
//
//
// 提示：
//
//
// n == height.length
// 1 <= n <= 2 * 10⁴
// 0 <= height[i] <= 10⁵
//
// Related Topics 栈 数组 双指针 动态规划 单调栈 👍 3563 👎 0

package leetcode.editor.cn;

class 接雨水 {
    public static void main(String[] args) {
        Solution solution = new 接雨水().new Solution();
        int[] height = new int[]{3,1,10,2,8};
        System.out.println(solution.trap(height));
    }

    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        public int trap(int[] height) {
            if (height == null || height.length < 2) {
                return 0;
            }

            //LR双指针
            int N = height.length;
            int L = 1;

            int leftMax = height[0];
            int R = N - 2;
            int rightMax = height[N - 1];
            int water = 0;
            while (L <= R) {
                if (leftMax <= rightMax) {
                    water += Math.max(0, leftMax - height[L]);
                    leftMax = Math.max(leftMax, height[L++]);
                } else {
                    water += Math.max(0, rightMax - height[R]);
                    rightMax = Math.max(rightMax, height[R--]);
                }
            }
            return water;
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}
