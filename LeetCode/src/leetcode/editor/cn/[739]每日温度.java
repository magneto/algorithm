//给定一个整数数组 temperatures ，表示每天的温度，返回一个数组 answer ，其中 answer[i] 是指对于第 i 天，下一个更高温度出现
//在几天后。如果气温在这之后都不会升高，请在该位置用 0 来代替。
//
//
//
// 示例 1:
//
//
//输入: temperatures = [73,74,75,71,69,72,76,73]
//输出: [1,1,4,2,1,1,0,0]
//
//
// 示例 2:
//
//
//输入: temperatures = [30,40,50,60]
//输出: [1,1,1,0]
//
//
// 示例 3:
//
//
//输入: temperatures = [30,60,90]
//输出: [1,1,0]
//
//
//
// 提示：
//
//
// 1 <= temperatures.length <= 10⁵
// 30 <= temperatures[i] <= 100
//
// Related Topics 栈 数组 单调栈 👍 1237 👎 0

package leetcode.editor.cn;

import java.util.*;

class 每日温度 {
    public static void main(String[] args) {
        每日温度.Solution solution = new 每日温度().new Solution();
        int[] temperatures = new int[]{73, 74, 75, 71, 69, 72, 76, 73};
        int[] rst = solution.dailyTemperatures(temperatures);
        Arrays.stream(rst).forEach(i -> System.out.print(i + " "));
        System.out.println();
    }

    //for (遍历这个数组)
    //{
    //    if (栈空 || 栈顶元素大于等于当前比较元素)
    //    {
    //        入栈;
    //    }
    //    else
    //    {
    //        while (栈不为空 && 栈顶元素小于当前元素)
    //        {
    //            栈顶元素出栈;
    //            更新结果;
    //        }
    //        当前数据入栈;
    //    }
    //}

    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        public int[] dailyTemperatures(int[] arr) {
            //单调栈
            if (arr == null || arr.length == 0) {
                return new int[0];
            }
            int N = arr.length;
            int[] ans = new int[N];
            Stack<List<Integer>> stack = new Stack<>();//栈
            //遍历这个数组
            for (int i = 0; i < N; i++) {
                while (!stack.isEmpty() && arr[stack.peek().get(0)] < arr[i]) {
                    List<Integer> popIs = stack.pop();
                    for (Integer popi : popIs) {
                        ans[popi] = i - popi;
                    }
                }
                if (!stack.isEmpty() && arr[stack.peek().get(0)] == arr[i]) {
                    stack.peek().add(Integer.valueOf(i));
                } else {
                    ArrayList<Integer> list = new ArrayList<>();
                    list.add(i);
                    stack.push(list);
                }
            }
            return ans;
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}
