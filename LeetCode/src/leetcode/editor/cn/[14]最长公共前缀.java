//编写一个函数来查找字符串数组中的最长公共前缀。
//
// 如果不存在公共前缀，返回空字符串 ""。
//
//
//
// 示例 1：
//
//
//输入：strs = ["flower","flow","flight"]
//输出："fl"
//
//
// 示例 2：
//
//
//输入：strs = ["dog","racecar","car"]
//输出：""
//解释：输入不存在公共前缀。
//
//
//
// 提示：
//
//
// 1 <= strs.length <= 200
// 0 <= strs[i].length <= 200
// strs[i] 仅由小写英文字母组成
//
// Related Topics 字符串 👍 2310 👎 0

package leetcode.editor.cn;

class 最长公共前缀 {
    public static void main(String[] args) {
        Solution solution = new 最长公共前缀().new Solution();
        //System.out.println(solution.longestCommonPrefix(new String[]{"abcdefd", "abcdedx", "abcdijn"}));
        System.out.println(solution.longestCommonPrefix(new String[]{""}));
    }

    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        public String longestCommonPrefix(String[] strs) {
            if (strs == null || strs.length == 0) {
                return "";
            }
            if (strs.length == 1 && strs[0].equals("")) {
                return "";
            }
            if (strs.length == 1) {
                return strs[0];
            }
            char[] chs = strs[0].toCharArray();
            int min = Integer.MAX_VALUE;
            //拿第0个做参考 从第一个开始比较到最后一个
            for (int i = 1; i < strs.length; i++) {
                char[] tmp = strs[i].toCharArray();

                int index = 0;
                while (index < chs.length && index < tmp.length) {
                    if (chs[index] != tmp[index]) {
                        break;
                    }
                    index++;
                }
                min = Math.min(min, index);
                if (min == 0) {
                    return "";
                }
            }
            return strs[0].substring(0, min);
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}
