//给你一个 32 位的有符号整数 x ，返回将 x 中的数字部分反转后的结果。
//
// 如果反转后整数超过 32 位的有符号整数的范围 [−2³¹, 231 − 1] ，就返回 0。
//假设环境不允许存储 64 位整数（有符号或无符号）。
//
//
//
// 示例 1：
//
//
//输入：x = 123
//输出：321
//
//
// 示例 2：
//
//
//输入：x = -123
//输出：-321
//
//
// 示例 3：
//
//
//输入：x = 120
//输出：21
//
//
// 示例 4：
//
//
//输入：x = 0
//输出：0
//
//
//
//
// 提示：
//
//
// -2³¹ <= x <= 2³¹ - 1
//
// Related Topics 数学 👍 3202 👎 0


package leetcode.editor.cn;

class 整数反转 {
    public static void main(String[] args) {
        Solution solution = new 整数反转().new Solution();
        System.out.println(Integer.MAX_VALUE);
        System.out.println(Integer.MIN_VALUE);
        int reverse = solution.reverse2(1534236469);
        System.out.println(reverse);
    }

    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        public int reverse(int x) {
            int rst = 0;
            while (x != 0) {
                int tmp = x % 10;

                //正数负数的溢出判断
                int positive = Integer.MAX_VALUE / 10;
                int negative = Integer.MIN_VALUE / 10;
                if (rst > positive || (rst == positive && tmp > 7)) {
                    return 0;
                }
                if (rst < negative || (rst == negative && tmp < -8)) {
                    return 0;
                }

                rst = rst * 10 + tmp;
                x = x / 10;
            }
            return rst;
        }

        public  int reverse2(int x) {
            boolean neg = ((x >>> 31) & 1) == 1;//负数 1
            x = neg ? x : -x; // 正数转为负数
            int m = Integer.MIN_VALUE / 10;
            int o = Integer.MIN_VALUE % 10;
            int res = 0;
            while (x != 0) {
                if (res < m || (res == m && x % 10 < o)) { //反转后超
                    return 0;
                }
                res = res * 10 + x % 10;
                x /= 10;
            }
            return neg ? res : Math.abs(res);
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}

