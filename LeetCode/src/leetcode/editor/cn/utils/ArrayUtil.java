package leetcode.editor.cn.utils;

import java.util.Arrays;

public class ArrayUtil {

    public static int[] generateAbsoluteRandomArray(int maxSize, int maxValue) {
        int[] arr = new int[maxSize];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = Math.abs((int) ((maxValue + 1) * Math.random()));
        }
        return arr;
    }

    public static void printArray(int[] arr) {
        Arrays.stream(arr).forEach(num -> System.out.print(num + " "));
        System.out.println();
    }
}
