//给你一个链表，删除链表的倒数第 n 个结点，并且返回链表的头结点。
//
//
//
// 示例 1：
//
//
//输入：head = [1,2,3,4,5], n = 2
//输出：[1,2,3,5]
//
//
// 示例 2：
//
//
//输入：head = [1], n = 1
//输出：[]
//
//
// 示例 3：
//
//
//输入：head = [1,2], n = 1
//输出：[1]
//
//
//
//
// 提示：
//
//
// 链表中结点的数目为 sz
// 1 <= sz <= 30
// 0 <= Node.val <= 100
// 1 <= n <= sz
//
//
//
//
// 进阶：你能尝试使用一趟扫描实现吗？
// Related Topics 链表 双指针 👍 2100 👎 0

package leetcode.editor.cn;

class 删除链表的倒数第N个结点 {
    public static void main(String[] args) {
        Solution solution = new 删除链表的倒数第N个结点().new Solution();
        ListNode head = new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(5, new ListNode(6, new ListNode(7, null)))))));
        ListNode node = solution.removeNthFromEnd(head, 3);
        while (node != null) {
            System.out.print(node.val + "  ");
            node = node.next;
        }
        System.out.println();
    }

    public static class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }
    //leetcode submit region begin(Prohibit modification and deletion)


    class Solution {
        public ListNode removeNthFromEnd(ListNode head, int n) {
            ListNode cur = head;
            ListNode pre = null;
            while (cur != null) {
                n--;
                cur = cur.next;
                if (n == -1) {
                    pre = head;
                }
                if (n < -1) {
                    pre = pre.next;
                }
            }
            if (n > 0) {
                return head;
            }
            if (pre == null) {
                return head.next;
            }
            pre.next = pre.next.next;
            return head;
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}
