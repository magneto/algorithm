package utils;

import java.util.Arrays;

public class ArrayUtil {
    /**
     * @Description: 数组打印工具类
     * @Version 1.0
     */
    public static void printArray(int[] arr) {
        Arrays.stream(arr).forEach(num -> {
            System.out.print(num + " ");
        });
        System.out.println();
    }

    public static void swap(int[] arr, int i, int j) {
        int tmp = arr[j];
        arr[j] = arr[i];
        arr[i] = tmp;
    }

    // 交换arr的i和j位置上的值 内存地址不同才能用
    public static void swap2(int[] arr, int i, int j) {
        arr[i] = arr[i] ^ arr[j];
        arr[j] = arr[i] ^ arr[j];
        arr[i] = arr[i] ^ arr[j];
    }

    /**
     * @Description: 生成随机数组 最大值为maxValue  最大长度maxSize
     */
    public static int[] generateRandomArray(int maxSize, int maxValue, boolean randomLength, boolean absolute) {
        int length = 0;
        if (randomLength) {
            length = (int) ((maxSize + 1) * Math.random());
        } else {
            length = maxSize;
        }
        int[] arr = new int[length];
        if (absolute) {
            for (int i = 0; i < arr.length; i++) {
                arr[i] = Math.abs((int) ((maxValue + 1) * Math.random()));
            }
        } else {
            for (int i = 0; i < arr.length; i++) {
                arr[i] = (int) ((maxValue + 1) * Math.random()) - (int) (maxValue * Math.random());
            }
        }

        return arr;
    }

    /**
     * @Description: 生成随机数组，且相邻数不相等
     */
    public static int[] generateRandomArrayNoEqual(int maxLen, int maxValue) {
        int len = (int) (Math.random() * maxLen);
        int[] arr = new int[len];
        if (len > 0) {
            arr[0] = (int) (Math.random() * maxValue);
            for (int i = 1; i < len; i++) {
                do {
                    arr[i] = (int) (Math.random() * maxValue);
                } while (arr[i] == arr[i - 1]);
            }
        }
        return arr;
    }

    /**
     * 两个数组是不是相等
     */
    public static boolean isEqual(int[] arr1, int[] arr2) {
        if ((arr1 == null && arr2 != null) || (arr1 != null && arr2 == null)) {
            return false;
        }
        if (arr1 == null && arr2 == null) {
            return true;
        }
        if (arr1.length != arr2.length) {
            return false;
        }

        //System.out.println("数组相等判断 开始 --------------------------------");
        //System.out.println("数组相等判断 arr1--------------------------------");
        //printArray(arr1);
        //System.out.println("数组相等判断 arr2--------------------------------");
        //printArray(arr2);
        //System.out.println("数组相等判断 结束 --------------------------------");

        for (int i = 0; i < arr1.length; i++) {
            if (arr1[i] != arr2[i]) {
                return false;
            }
        }
        return true;
    }

    /**
     * 两个数组是不是反转的
     */
    public static boolean isRevere(int[] headArray, int[] reverseArray) {
        printArray(headArray);
        printArray(reverseArray);
        for (int i = 0; i < headArray.length; i++) {
            if (headArray[i] != reverseArray[headArray.length - i - 1]) {
                return false;
            }
        }
        return true;
    }

    /**
     * 数组复制方法
     */
    public static int[] copyArray(int[] arr) {
        if (arr == null) {
            return null;
        }
        int[] res = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            res[i] = arr[i];
        }
        return res;
    }
}
