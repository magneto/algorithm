package class02;

import utils.ListNode;

/**
 * @Description: Code02_单链表删除节点
 */
public class Code02_单链表删除节点 {
    public static ListNode removeValue(ListNode head, int num) {
        while (head != null) {
            if (head.value != num) {
                break;
            }
            head = head.next;
        }
        ListNode pre = head;
        ListNode cur = head;
        while (cur != null) {
            if (cur.value == num) {
                pre.next = cur.next;
            } else {
                pre = cur;
            }
            cur = cur.next;
        }
        return head;
    }
}
