package class01;

import utils.ArrayUtil;

import java.util.Arrays;

public class Code01_选择排序 {

    public static void selectionSort(int[] arr) {
        if (arr == null || arr.length < 2) {
            return;
        }
        // 0～n-1
        // 1～n-1
        // 2～n-1
        for (int i = 0; i < arr.length - 1; i++) { // i ~ N-1 每次排序的范围
            // 最小值在哪个位置上  i～n-1
            int minIndex = i;
            for (int j = i + 1; j < arr.length; j++) { // i ~ N-1 上找最小值的下标
                minIndex = arr[j] < arr[minIndex] ? j : minIndex;
            }
            ArrayUtil.swap(arr, i, minIndex);
        }
    }

    // for test
    public static void main(String[] args) {
        int testTime = 500000;
        int maxSize = 100;
        int maxValue = 100;
        boolean succeed = true;
        for (int i = 0; i < testTime; i++) {
            int[] arr1 = ArrayUtil.generateRandomArray(maxSize, maxValue, false, true);
            int[] arr2 = ArrayUtil.copyArray(arr1);
            selectionSort(arr1);
            Arrays.sort(arr2);
            if (!ArrayUtil.isEqual(arr1, arr2)) {
                succeed = false;
                ArrayUtil.printArray(arr1);
                ArrayUtil.printArray(arr2);
                break;
            }
        }
        System.out.println(succeed ? "Nice!" : "Fucking fucked!");
    }
}
