package chapter06;

import java.util.*;

/**
 * @ClassName BreadthFristSearch
 * @Description: 广度优先搜索
 * @Author: WangWenpeng
 * @date: 19:59 2021/8/30
 * @Version 1.0
 */
public class BreadthFristSearch {
    public static void main(String[] args) {
        HashMap<String, List<String>> graph = new HashMap<String, List<String>>();
        graph.put("you", new LinkedList<String>(Arrays.asList("alice", "bob", "claire")));
        graph.put("bob", new LinkedList<String>(Arrays.asList("anuj", "peggy")));
        graph.put("alice", new LinkedList<String>(Arrays.asList("peggy")));
        graph.put("claire", new LinkedList<String>(Arrays.asList("thom", "jonny")));
        graph.put("anuj", new LinkedList<String>());
        graph.put("peggy", new LinkedList<String>());
        graph.put("thom", new LinkedList<String>());
        graph.put("jonny", new LinkedList<String>());

        LinkedList<String> search_queue = new LinkedList<String>();
        LinkedList<String> searched = new LinkedList<String>();

        search_queue.addAll(graph.get("you"));
        while (!search_queue.isEmpty()) {
            String person = search_queue.poll();
            //搜索过的不再搜
            if (!searched.contains(person)) {
                if (person_is_seller(person)) {
                    System.out.println(person + " is a mango seller!");
                    return;
                } else {
                    //二级 三级 n级朋友加入搜索
                    search_queue.addAll(graph.get(person));
                    //已经搜索过的加入searched 不再搜索
                    searched.add(person);
                }
            }
        }
        System.out.println("there is no mango seller!");
    }

    /**
     * @Description 单纯的假设以m结尾是芒果销售
     */
    private static boolean person_is_seller(String name) {
        return name.endsWith("m");
    }
}
