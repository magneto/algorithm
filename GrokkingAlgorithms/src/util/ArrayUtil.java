package util;

/**
 * @ClassName ArrayUtil
 * @Description: 数组工具类
 * @Author: WangWenpeng
 * @date: 10:34 2021/8/29
 * @Version 1.0
 */
public class ArrayUtil {

    /**
     * 数组打印
     */
    public static void printArray(int array[]) {
        for (int i : array) {
            System.out.print(" " + i);
        }
        System.out.println();
    }

    /**
     * 数组交换
     */
    public static void swap(int[] arr, int i, int j) {
        int tmp = arr[j];
        arr[j] = arr[i];
        arr[i] = tmp;
    }
}
