package chapter02;

import util.ArrayUtil;

/**
 * @ClassName SelectSort
 * @Description: 选择排序
 * @Author: WangWenpeng
 * @date: 10:47 2021/8/29
 * @Version 1.0
 */
public class SelectSort {
    public static void main(String[] args) {
        int[] array = {5, 4, 7, 8, 1, 3, 7, 9, 15, 24};
        ArrayUtil.printArray(array);
        selectSort(array);
        ArrayUtil.printArray(array);
    }

    public static void selectSort(int[] array) {
        if (array == null || array.length < 2) {
            return;
        }
        //这里length存储长度是为了for中不再去计算长度 提高效率
        int length = array.length;
        for (int i = 0; i < length; i++) {
            int minValueIndex = i;
            //找一个最小值 不断更新最小值的index
            for (int j = i + 1; j < length; j++) {
                minValueIndex = array[minValueIndex] < array[j] ? minValueIndex : j;
            }
            ArrayUtil.swap(array, i, minValueIndex);
        }
    }
}
