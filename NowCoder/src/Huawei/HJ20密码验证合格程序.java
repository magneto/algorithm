package Huawei;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HJ20密码验证合格程序 {
    public static void main(String[] args) throws IOException {
        BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
        String s;
        while ((s = bf.readLine())!=null) {
            // 长度check
            if (s.length() <= 8 ) {
                System.out.println("NG");
                continue;
            }

            // 大小写字母.数字.其它符号check
            String[] regexes = {"\\d", "[a-z]", "[A-Z]", "[~!@#$%^&*()_+=<>?/*-]"};
            int types = 0;
            for (String re : regexes) {
                Pattern p = Pattern.compile(re);
                Matcher m = p.matcher(s);
                if (m.find()) {
                    types += 1;
                }
            }
            if(types<3){
                System.out.println("NG");
                continue;
            }


            // 3.不能有长度大于2的包含公共元素的子串重复
            boolean flag = false;
            for (int i = 1; i < s.length() - 2; i++) {
                String sub = s.substring(i - 1, i + 2);
                String s1 = s.substring(0, i);
                String s2 = s.substring(i + 2);
                if (s1.indexOf(sub) != -1) { // 存在相同字符串
                    flag = true;
                    System.out.println("NG");
                    break;
                }
                if (s2.indexOf(sub) != -1) { // 存在相同字符串
                    flag = true;
                    System.out.println("NG");
                    break;
                }
            }
            // 4.打印结果
            if(!flag){
                System.out.println("OK");
            }
        }
    }
}
