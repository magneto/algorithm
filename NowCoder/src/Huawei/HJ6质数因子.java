package Huawei;

import java.util.Scanner;

public class HJ6质数因子 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        long num = scanner.nextLong();
        double sqrt = Math.sqrt(num);
        for (int i = 2; i <= sqrt; i++) {
            while (num % i == 0) {
                System.out.print(i + " ");
                num = num / i;
            }
        }
        System.out.println(num == 1 ? "" : num + " ");
    }
}
