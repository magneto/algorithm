package Huawei;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class HJ21简单密码 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        String res = "";
        Map<Character, Character> lowerMap = lowerMap();
        for (int i = 0; i < s.length(); ++i) {
            if (lowerMap.get(s.charAt(i)) != null) {
                res += lowerMap.get(s.charAt(i));
            } else if (s.charAt(i) >= 'A' && s.charAt(i) <= 'Y') {
                res += (char) (s.charAt(i) + 33);
            } else if (s.charAt(i) == 'Z') res += 'a';
            else res += s.charAt(i);
        }
        System.out.println(res);
    }

    private static Map<Character, Character> lowerMap() {
        Map<Character, Character> map = new HashMap<>();
        map.put('a', '2');
        map.put('b', '2');
        map.put('c', '2');
        map.put('d', '3');
        map.put('e', '3');
        map.put('f', '3');
        map.put('g', '4');
        map.put('h', '4');
        map.put('i', '4');
        map.put('j', '5');
        map.put('k', '5');
        map.put('l', '5');
        map.put('m', '6');
        map.put('n', '6');
        map.put('o', '6');
        map.put('p', '7');
        map.put('q', '7');
        map.put('r', '7');
        map.put('s', '7');
        map.put('t', '8');
        map.put('u', '8');
        map.put('v', '8');
        map.put('w', '9');
        map.put('x', '9');
        map.put('y', '9');
        map.put('z', '9');
        return map;
    }
}
