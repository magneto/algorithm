package Huawei;

import java.util.Arrays;
import java.util.Scanner;

public class HJ45名字的漂亮度 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        in.nextLine();
        for (int i = 0; i < n; i++) {
            System.out.println(getScore(in.nextLine()));
        }
    }

    public static int getScore(String name) {
        int score = 0;
        char[] cha = name.toCharArray();
        int[] count = new int[26];
        for (int i = 0; i < cha.length; i++) {
            count[Character.toLowerCase(cha[i]) - 'a']++;//统计每个字母出现的次数，忽略大小写
        }
        Arrays.sort(count);//升序排列
        for (int i = 1; i <= 26; i++) {//计算漂亮度
            score += i * count[i - 1];
        }
        return score;
    }
}
