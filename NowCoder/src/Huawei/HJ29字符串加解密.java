package Huawei;

import java.util.Scanner;

public class HJ29字符串加解密 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            System.out.println(encrypt(in.nextLine()));
            System.out.println(decrypt(in.nextLine()));
        }
    }

    public static String encrypt(String s) {
        char[] cs = s.toCharArray();
        //foreach拿到的不是元素本身，而是一个新变量，所以这里不能用foreach
        //除非新建一个StringBuilder
        for (int i = 0; i < s.length(); i++) {
            if (cs[i] >= 'a' && cs[i] <= 'z') {
                cs[i] = (char) ((cs[i] - 'a' + 1) % 26 + 'A');
            } else if (cs[i] >= 'A' && cs[i] <= 'Z') {
                cs[i] = (char) ((cs[i] - 'A' + 1) % 26 + 'a');
            } else if (cs[i] >= '0' && cs[i] <= '9') {
                cs[i] = (char) ((cs[i] - '0' + 1) % 10 + '0');
            }
        }
        return String.valueOf(cs);
    }

    public static String decrypt(String s) {
        char[] cs = s.toCharArray();
        //foreach拿到的不是元素本身，而是一个新变量，所以这里不能用foreach
        //除非新建一个StringBuilder
        //注意减法要再加一个26或10，才可以正常循环起来
        for (int i = 0; i < s.length(); i++) {
            if (cs[i] >= 'a' && cs[i] <= 'z') {
                cs[i] = (char) ((cs[i] - 'a' - 1 + 26) % 26 + 'A');
            } else if (cs[i] >= 'A' && cs[i] <= 'Z') {
                cs[i] = (char) ((cs[i] - 'A' - 1 + 26) % 26 + 'a');
            } else if (cs[i] >= '0' && cs[i] <= '9') {
                cs[i] = (char) ((cs[i] - '0' - 1 + 10) % 10 + '0');
            }
        }
        return String.valueOf(cs);
    }
}
