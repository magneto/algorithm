package Huawei;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class HJ14字符串排序 {
    public static void main(String[] args) throws IOException {
        List<String> list = new ArrayList<>();
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        br.readLine();
        String s;
        while ((s = br.readLine()) != null) {
            list.add(s);
        }
        br.close();

        list = list.stream().sorted().collect(Collectors.toList());
        list.forEach(System.out::println);
    }
}
