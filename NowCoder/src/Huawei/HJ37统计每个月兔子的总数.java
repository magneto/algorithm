package Huawei;

import java.util.Scanner;

public class HJ37统计每个月兔子的总数 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            int n = sc.nextInt();
            System.out.println(dp(n));
        }
    }

    public static int dp(int n) {
        int num[] = new int[n + 1];
        num[1] = 1;
        num[2] = 1;
        for (int i = 3; i <= n; i++) {
            num[i] = num[i - 1] + num[i - 2];
        }
        return num[n];
    }
}
