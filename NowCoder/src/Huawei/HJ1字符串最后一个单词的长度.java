package Huawei;

public class HJ1字符串最后一个单词的长度 {
    public static void main(String[] args) {
        int length = getLength("qwer ghj");
        System.out.println(length);
    }

    public static int getLength(String str) {
        if (str == null || str.length() == 0) {
            return 0;
        }
        String[] s = str.split(" ");
        int length = s[s.length - 1].length();
        return length;
    }

}
