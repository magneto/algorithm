package Huawei;

import java.util.Scanner;

public class HJ105记负均正II {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int temp = 0;
        int countN = 0;
        int countP = 0;
        double sum = 0.0;

        while (in.hasNextInt()) {
            temp = in.nextInt();
            if (temp < 0) {
                countN++;
            } else {
                countP++;
                sum += temp;
            }
        }
        System.out.println(countN);
        if (countP == 0) {
            System.out.printf("0.0");
        } else {
            System.out.printf("%.1f\n", sum / countP);
        }
    }
}
