package Huawei;

import java.util.Scanner;

public class HJ51输出单向链表中倒数第k个结点 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            int n = Integer.parseInt(sc.next());
            ListNode head = new ListNode(-1);
            ListNode temp = head;
            //生成链表
            for (int i = 0; i < n; i++) {
                ListNode node = new ListNode(sc.nextInt());
                temp.next = node;
                temp = temp.next;
            }
            int k = Integer.parseInt(sc.next());
            //使用快慢指针
            if (getKthFromEnd(head.next, k) != null) {
                System.out.println(getKthFromEnd(head.next, k).val);
            } else {
                System.out.println(0);
            }

        }
    }

    //通过快慢指针搜索
    public static ListNode getKthFromEnd(ListNode head, int k) {
        if (head == null) return null;

        ListNode fast = head, slow = head;

        //快指针先走k步
        for (int i = 0; i < k; i++) {
            if (fast == null) return fast;
            fast = fast.next;
        }
        while (fast != null) {
            fast = fast.next;
            slow = slow.next;
        }
        return slow;
    }
}

class ListNode {
    ListNode next;
    int val;

    ListNode(int val) {
        this.val = val;
        next = null;
    }
}
