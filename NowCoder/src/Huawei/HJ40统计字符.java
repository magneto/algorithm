package Huawei;

import java.util.Scanner;

public class HJ40统计字符 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            String s = scanner.nextLine();
            //统计其中英文字符，空格字符，数字字符，其他字符的个数
            int letters = 0;
            int spaces = 0;
            int digits = 0;
            int others = 0;
            int len = s.length();
            for (int i = 0; i < len; i++) {
                char c = s.charAt(i);
                if (Character.isLetter(c)) {
                    letters++;
                } else if (Character.isDigit(c)) {
                    digits++;
                } else if (Character.isSpaceChar(c)) {
                    spaces++;
                } else {
                    others++;
                }
            }
//            统计其中英文字符，空格字符，数字字符，其他字符的个数
            System.out.println(letters);
            System.out.println(spaces);
            System.out.println(digits);
            System.out.println(others);
        }
    }
}
