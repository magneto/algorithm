package Huawei;

import java.util.Scanner;

public class HJ11数字颠倒 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        StringBuffer ans = new StringBuffer();
        while (n / 10 != 0) {
            ans.append(n % 10);
            n = n / 10;
        }
        ans.append(n);
        System.out.print(ans);
    }
}
