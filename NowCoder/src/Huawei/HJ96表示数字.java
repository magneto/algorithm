package Huawei;

import java.util.Scanner;

public class HJ96表示数字 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNextLine()) {
            String pInStr = in.nextLine();
            String res = markNum(pInStr);
            System.out.println(res);
        }
        in.close();
    }

    private static String markNum(String pInStr) {
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < pInStr.length(); ) {
            // 若发现了数字
            if (pInStr.charAt(i) >= '0' && pInStr.charAt(i) <= '9') {
                res.append('*');
                while (i < pInStr.length() && pInStr.charAt(i) >= '0' && pInStr.charAt(i) <= '9') {
                    res.append(pInStr.charAt(i++));
                }
                res.append('*');
            } else {
                res.append(pInStr.charAt(i++));
            }
        }
        return res.toString();
    }
}
