package Huawei;

import java.util.Scanner;

public class HJ108求最小公倍数 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int num = gcd(a, b);
        System.out.println(a * b / num);
    }

    private static int gcd(int a, int b) {
        int big = a > b ? a : b;
        int small = big == a ? b : a;
        if (small == 0) {
            return big;
        }
        if (big % small == 0) {
            return small;
        }
        return gcd(big % small, small);
    }
}
