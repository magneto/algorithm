package Huawei;

import java.util.Scanner;
import java.util.Stack;

public class HJ12字符串反转 {
    public static void main(String[] args){
        Stack stack = new Stack();
        Scanner in = new Scanner(System.in);
        String s = in.nextLine();
        for (int i = 0; i <s.trim().length() ; i++) {
            stack.push(s.charAt(i));
        }
        while(!stack.empty()){
            System.out.print(stack.pop());
        }
    }
}
