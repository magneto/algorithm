package Huawei;

import java.util.Scanner;

public class HJ15求int型正整数在内存中存储时1的个数 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int num = in.nextInt();
        int n = 0;
        for (int i = 0; i < 32; i++) {
            if ((num & 1) == 1)
                n++;
            num = num >>> 1;
        }
        System.out.println(n);
    }
}
