package Huawei;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class HJ107求解立方根 {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        Double num = Double.valueOf(bufferedReader.readLine());
        Double rst = deal(num);
        System.out.printf("%.1f", rst);
    }

    private static Double deal(double num) {
        double left, right, mid, three;
        left = Math.min(-1.0, num);
        right = Math.max(1.0, num);
        while (right - left > 0.001) {
            mid = (right + left) / 2;
            three = mid * mid * mid;
            if (three > num) {
                right = mid;
            } else if (three < num) {
                left = mid;
            } else {
                return mid;
            }
        }
        return left;
    }
}
