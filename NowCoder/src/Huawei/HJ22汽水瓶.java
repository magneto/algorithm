package Huawei;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class HJ22汽水瓶 {
    public static void main(String[] args) throws IOException {
        m1();
        m2();
    }

    private static void m2() throws IOException {
        BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
        String s;
        while ((s = bf.readLine()) != null) {
            int num = Integer.parseInt(s);
            if (num == 0) {
                return;
            }
            int count = 0;
            while (num / 3 > 0) {
                count += num / 3;
                //此时还有多少瓶盖
                num = num / 3 + num % 3;
                //可以向老板借一个
                if (num == 2) {
                    num = num + 1;
                }
            }
            System.out.println(count);
        }
        bf.close();
    }

    private static void m1() throws IOException {
        BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
        String s;
        while ((s = bf.readLine()) != null) {
            int num = Integer.parseInt(s);
            if (num == 0) {
                return;
            }
            System.out.println(num / 2);
        }
        bf.close();
    }
}
