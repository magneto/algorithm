package Huawei;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class HJ23删除字符串中出现次数最少的字符 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNextLine()) {
            String str = in.nextLine();
            String res = deleteRepeatStr(str);
            System.out.println(res);
        }
        in.close();
    }

    private static String deleteRepeatStr(String str) {
        Map<Character, Integer> map = new HashMap<>();
        for (char c : str.toCharArray()) {
            map.put(c, map.getOrDefault(c, 0) + 1);
        }

        int min = Integer.MAX_VALUE;
        for (Integer value : map.values()) {
            min = Math.min(min, value);
        }
        //Integer min = map.values().stream().sorted().findFirst().orElse(Integer.MAX_VALUE);

        StringBuilder res = new StringBuilder();
        for (char ch : str.toCharArray()) {
            if (map.get(ch) != min) {
                res.append(ch);
            }
        }
        return res.toString();
    }
}
