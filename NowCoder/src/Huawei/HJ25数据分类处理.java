package Huawei;

import java.util.*;

public class HJ25数据分类处理 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            int In = scanner.nextInt(); //整数序列I的个数
            String[] I_arr = new String[In];
            for (int i = 0; i < In; i++) {
                I_arr[i] = String.valueOf(scanner.nextInt());
            }

            int Rn = scanner.nextInt();//规则整数序列R的个数
            Set<Integer> R_set = new TreeSet<>();//使用TreeSet进行排序和去重
            for (int i = 0; i < Rn; i++) {
                R_set.add(scanner.nextInt());
            }

            List<Integer> I_list = new ArrayList<>();//用于存储整数序列I
            List<Integer> R_list = new ArrayList<>();//用于存储规则整数序列R
            for (Integer r_item : R_set) {
                int count = 0;//统计R中元素在I中出现的次数
                for (int i = 0; i < I_arr.length; i++) {
                    if (I_arr[i].contains(String.valueOf(r_item))) {
                        count++;
                        I_list.add(i);
                        I_list.add(Integer.valueOf(I_arr[i]));
                    }
                }
                if (count > 0) {
                    R_list.add(r_item);
                    R_list.add(count);
                    R_list.addAll(I_list);
                }
                I_list.clear();
            }
            System.out.print(R_list.size() + " ");
            R_list.stream().forEach(i -> System.out.print(i + " "));
            System.out.println();
            System.out.println();
        }
        scanner.close();
    }
}
