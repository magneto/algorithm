package Huawei;

public class HJ2计算某字符出现次数 {
    public static void main(String[] args) {
        int count = getCount("abcdea", "a");
        System.out.println(count);
    }

    public static int getCount(String origin, String target) {
        return origin.length() - origin.toUpperCase().replaceAll(target.toUpperCase(), "").length();
    }
}
